const config = require('../config.json')

// get url string
function extractUrl(html, index) {
    const regexp = new RegExp(/var sFiles\="([^;]+)";/)
    const res = regexp.exec(html)

    if (!res || res.length < 2) return []

    // build absloute url path
    const urlSegment = res[1].split('|')
    return urlSegment.map(url => `${config.base[index]}${url}`)
}

// get title string
function extractName(html) {
    const regexp = new RegExp(/<span id='spt2'>(.+)<\/span>/)
    const res = regexp.exec(html)
    if (!res || res.length < 2) return ''
    return res[1].trim()
}

function extractUrlBase(html) {
    const regexp = new RegExp(/var sPath\="(.+)";/)
    const res = regexp.exec(html)
    if (!res || res.length < 2) return ''
    let index = (parseInt(res[1].trim()) || 0) - 1
    return index < 0 ? 0 : index
}


module.exports = {
    extractUrl,
    extractName,
    extractUrlBase,
}

