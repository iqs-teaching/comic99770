const fs = require('fs')

// write to real file
function write(name, data) {
    fs.writeFileSync(`${__dirname}'/../page_sample/${name}`, data)
}


// read all text
function read(name) {
    return fs.readFileSync(`${__dirname}'/../page_sample/${name}`)
}


// create dir in path
function mkdir(path, name) {

    // make sure path is exist
    if (!fs.existsSync(path)) throw `Path is not exists: ${path}`
    
    // stop words filter
    name = name.replace(/[\\\/\:\*\?"<>\|]/g, ' ').trim()

    // make sure dir name is not null or empty
    if (!name) name = new Date().getTime().toString()

    let dirpath = `${path}/${name}`
    
    // if dir is exist, add suffix otherwise create it
    if (!fs.existsSync(dirpath)) {
        fs.mkdirSync(dirpath)
    }
    else {
        let i = 1
        while (true) {
            dirpath = `${path}/${name} (${i})`
            if (fs.existsSync(dirpath)) {
                i++;
                continue;
            }
            fs.mkdirSync(dirpath)
            break;
        }
    }
    return dirpath
}


module.exports = {
    write,
    read,
    mkdir
}
