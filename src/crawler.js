const fs = require('fs')
const request = require('request-promise')

// get web page (html)
function req(url, extoptions = {}) {

	console.log(`HTTP GET: ${url}`)

	const defaultoptions = {
		url,
		method: 'GET',
		headers: {
			'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36'
		}
	}

	return request(Object.assign(defaultoptions, extoptions))
}

// download url(image)
async function download(url, path) {

	try {
		await req(url).pipe(fs.createWriteStream(path))
		return true
	}
	catch (e) {
		console.error(e)
		return false
	}
}


module.exports = {
	req,
	download,
}