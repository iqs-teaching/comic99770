const crawler = require('./src/crawler')
const file = require('./src/file')
const comic = require('./src/comic')

// get url
if(!process.argv[2]) return console.warn('Need to specify a 99770 comic URL !!')
const url = process.argv[2]


// run crawler and downloader
;(async ()=>{

    try{
        // get html
        const html = await crawler.req(url)
        const name =  comic.extractName(html)
        const baseUrlIndex = comic.extractUrlBase(html)
        const urls = comic.extractUrl(html,baseUrlIndex)
        

        console.log(`Ready to download: ${name}`)

        // mkdir for store comic images
        let dir = file.mkdir(`${__dirname}/comicbook`,name)    
        
        // download image one by one
        for(let i=0;i<urls.length;i++){
            let path = `${dir}/${i+1}.png`
            await crawler.download(urls[i],path)
        }
    }
    catch(e){
        console.log(e)        
    }

})()