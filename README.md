# Comic 99770 Crawler

這是一個教學用的 Node.js 爬蟲程式，它用來下載 [99770漫畫](http://99770.hhxxee.com/) 網站上的漫畫，以一集為單位下載


## Launch

啟動專案

```
git clone https://gitlab.com/iqs-teaching/comic99770.git
cd comic99770
npm install
```

## Usage

首先你必須前往 [99770漫畫](http://99770.hhxxee.com/) 選定一集漫畫，接著將 URL 複製起來，它會像是這樣：

```
http://99770.hhxxee.com/comic/10492/176146/?p=2&s=8
```

接著就可以開始下載：

```
node index.js "http://99770.hhxxee.com/comic/10492/176146/?p=2&s=8"
```

下載後圖片會放置在專案下的 `comicbook` 目錄下  

## Demo

以下是實際展示的成果預覽圖

#### 下載漫畫

![DEMO1](https://gitlab.com/iqs-teaching/comic99770/raw/master/1.PNG)

#### 整本完成下載

![DEMO2](https://gitlab.com/iqs-teaching/comic99770/raw/master/2.PNG)

## LICENSE BSD

    BSD LICENSE
